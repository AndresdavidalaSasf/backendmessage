package com.api.pv7back.Security.filters;

import com.api.pv7back.Entity.Usuarios;
import com.api.pv7back.Repository.UsuarioRepository;
import com.api.pv7back.Security.jwt.JwtUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class jwtAuthenticationFilters extends UsernamePasswordAuthenticationFilter {
    private JwtUtils jwtUtils;
    public jwtAuthenticationFilters(JwtUtils jwtUtils, UsuarioRepository repository){
        this.jwtUtils = jwtUtils;
        this.repository = repository;
    }

    private final UsuarioRepository repository;
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response) throws AuthenticationException {
        Usuarios usuarioEntity = null;
        String username = "";
        String password = "";
        try {

            usuarioEntity = new ObjectMapper().readValue(request.getInputStream(), Usuarios.class);
            username = usuarioEntity.getEmail();
            password = usuarioEntity.getPassword();
        }catch (Exception e){
            throw new RuntimeException(e);
        }
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(username, password);
        return getAuthenticationManager().authenticate(authenticationToken);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {
        User user = (User) authResult.getPrincipal();
        String token = jwtUtils.generateAccesToke(user.getUsername());

        Optional<Usuarios> userDto = repository.findByEmail(user.getUsername());


        response.addHeader("Authorization", token);

        Map<String, Object> httpResponse = new HashMap<>();
        httpResponse.put("token", token);
        httpResponse.put("Message", "Autenticacion Correcta");
        httpResponse.put("Username", user.getUsername());
        httpResponse.put("Body", userDto.get());
//        httpResponse.put(usuario);


        response.getWriter().write(new ObjectMapper().writeValueAsString(httpResponse));
        response.setStatus(HttpStatus.OK.value());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.getWriter().flush();
        super.successfulAuthentication(request, response, chain, authResult);
    }
}
