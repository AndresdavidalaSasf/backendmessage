package com.api.pv7back;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import static org.springframework.boot.SpringApplication.*;

@SpringBootApplication
public class Pv7backApplication {

	public static void main(String[] args) {
		run(Pv7backApplication.class, args);
	}

}
