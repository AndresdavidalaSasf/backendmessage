package com.api.pv7back.Repository;

import com.api.pv7back.Entity.Mensajes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MensajeRepository extends JpaRepository<Mensajes, Long> {
}
