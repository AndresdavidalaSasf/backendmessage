package com.api.pv7back.Repository;

import com.api.pv7back.Entity.Usuarios;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuarios, Long> {
    Usuarios findUsuariosByPasswordAndEmail(String password, String usuario);

    Optional<Usuarios> findByEmail(String email);
}
