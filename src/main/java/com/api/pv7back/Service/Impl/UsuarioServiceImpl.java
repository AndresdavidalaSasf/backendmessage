package com.api.pv7back.Service.Impl;

import com.api.pv7back.Entity.Usuarios;
import com.api.pv7back.Repository.UsuarioRepository;
import com.api.pv7back.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsuarioServiceImpl  implements UsuarioService {
    private final UsuarioRepository usuarioRepository;


    private PasswordEncoder passwordEncoder;
    @Autowired
    public UsuarioServiceImpl(UsuarioRepository usuarioRepository, PasswordEncoder passwordEncoder) {
        this.usuarioRepository = usuarioRepository;
        this.passwordEncoder = passwordEncoder;
    }


    @Override
    public List<Usuarios> getAllUsuarios() {
        return usuarioRepository.findAll();
    }

    @Override
    public Usuarios createUsers(Usuarios usuarios) {
        String encryptedPassword = passwordEncoder.encode(usuarios.getPassword());

        usuarios.setPassword(encryptedPassword);
        return usuarioRepository.save(usuarios);
    }

    @Override
    public Usuarios loginUser(String password, String email) {
        Usuarios usuarios = usuarioRepository.findUsuariosByPasswordAndEmail(password, email);
        if(usuarios == null){
            throw  new RuntimeException("excepcion");
        }
        return usuarios;
    }

    @Override
    public Usuarios updateUsuario(Long id, Usuarios usuarios) {
        Usuarios usuario = usuarioRepository.findById(id).orElseThrow();
        usuario.setFirstName(usuarios.getFirstName());
        usuario.setLastName(usuarios.getLastName());
        usuario.setAge(usuarios.getAge());
        usuario.setPhone(usuarios.getPhone());
        usuario.setEmail(usuarios.getEmail());
        usuario.setPassword(usuarios.getPassword());
        usuario.setImage(usuarios.getImage());
        usuario.setBirthDate(usuarios.getBirthDate());
        usuario.setAddress(usuarios.getAddress());
        usuario.setCity(usuarios.getCity());
        usuario.setRole(usuarios.getRole());

        return usuarioRepository.save(usuario);
    }

    @Override
    public Usuarios getOneUser(Long id) {
        return usuarioRepository.findById(id).orElseThrow();
    }

    @Override
    public void deleteUser(Long id) {
        Usuarios user = usuarioRepository.findById(id).orElseThrow();
        usuarioRepository.delete(user);

    }

}
