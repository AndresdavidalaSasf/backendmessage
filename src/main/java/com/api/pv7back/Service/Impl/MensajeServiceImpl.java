package com.api.pv7back.Service.Impl;

import com.api.pv7back.Entity.Mensajes;
import com.api.pv7back.Repository.MensajeRepository;
import com.api.pv7back.Service.MensajeService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
@Service
public class MensajeServiceImpl implements MensajeService {
    private final MensajeRepository mensajeRepository;

    public MensajeServiceImpl(MensajeRepository mensajeRepository) {
        this.mensajeRepository = mensajeRepository;
    }

    @Override
    public Mensajes createMensaje(Mensajes mensajes) {
        mensajes.setFechaEnvio(LocalDate.now());

        Mensajes nuevoMensaje = mensajeRepository.save(mensajes);
        return nuevoMensaje;
    }

    @Override
    public List<Mensajes> getAllMensajes() {
        return mensajeRepository.findAll();
    }

    @Override
    public Mensajes updeteMessage(Long id, Mensajes mensajes) {
        Mensajes getMensaje = mensajeRepository.findById(id).orElseThrow();

        getMensaje.setTexto(mensajes.getTexto());
        getMensaje.setFechaEnvio(mensajes.getFechaEnvio());
//        getMensaje.setAutor(mensajes.getAutor());
        return mensajeRepository.save(getMensaje);
    }

    @Override
    public Mensajes getOneMessage(Long id) {
        return mensajeRepository.findById(id).orElseThrow();
    }


}
