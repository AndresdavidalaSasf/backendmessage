package com.api.pv7back.Service;

import com.api.pv7back.Entity.Mensajes;
import org.springframework.stereotype.Service;

import java.util.List;

public interface MensajeService {
    Mensajes createMensaje(Mensajes mensajes);

    List<Mensajes> getAllMensajes();

    Mensajes updeteMessage(Long id, Mensajes mensajes);

    Mensajes getOneMessage(Long id);
}
