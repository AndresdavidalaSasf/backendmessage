package com.api.pv7back.Service;

import com.api.pv7back.Entity.Usuarios;

import java.util.List;

public interface UsuarioService {

    List<Usuarios> getAllUsuarios();

    Usuarios createUsers(Usuarios usuarios);

    Usuarios loginUser(String password, String email);

    Usuarios updateUsuario(Long id, Usuarios usuarios);

    Usuarios getOneUser(Long id);

    void deleteUser(Long id);
}
