package com.api.pv7back.Controller;


import com.api.pv7back.Entity.Mensajes;
import com.api.pv7back.Service.MensajeService;
import com.api.pv7back.Service.UsuarioService;
import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/")
@CrossOrigin(origins = "*")

public class MensajeriaController {
    private final MensajeService mensajeService;

    public MensajeriaController(MensajeService mensajeService) {
        this.mensajeService = mensajeService;
    }

    @GetMapping("mensajeria")
    public ResponseEntity<List<Mensajes>>getAllMensajes(){
        return ResponseEntity.ok(mensajeService.getAllMensajes());
    }

    @PostMapping("mensajeria/create")
    public ResponseEntity<Mensajes>postMensajeria(@RequestBody Mensajes mensajes){
        return ResponseEntity.ok(mensajeService.createMensaje(mensajes));
    }

    @PutMapping("mensajeria/update/{id}")
    public ResponseEntity<Mensajes> updateMessage(@PathVariable Long id, @RequestBody Mensajes mensajes){
        return  ResponseEntity.ok(mensajeService.updeteMessage(id, mensajes));
    }
    
    @GetMapping("mensajeria/{id}")
    public ResponseEntity<Mensajes> getOneMeessage(@PathVariable Long id){
        return ResponseEntity.ok(mensajeService.getOneMessage(id));
    }
}
