package com.api.pv7back.Controller;

import com.api.pv7back.Entity.Usuarios;
import com.api.pv7back.Service.UsuarioService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/")
@CrossOrigin(origins = "*")
public class usuariosController {
    private final UsuarioService usuarioService;

    @Autowired
    public usuariosController(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }



//    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/usuarios")
    public ResponseEntity<List<Usuarios>> getAllUsers(){
        return ResponseEntity.ok(usuarioService.getAllUsuarios());
    }



    @PostMapping("/usuarios/create")
    public ResponseEntity<Usuarios> createUsuarios(@RequestBody @Valid Usuarios usuarios){
        return ResponseEntity.ok(usuarioService.createUsers(usuarios));
    }

//    @PostMapping("/usuarios/login")
//    public ResponseEntity<Usuarios> login(@RequestBody Usuarios usuarios){
//        return ResponseEntity.ok(usuarioService.loginUser(usuarios.getPassword(), usuarios.getEmail()));
//    }

    @PutMapping("/usuarios/{id}")
    public ResponseEntity<Usuarios> updateUser(@PathVariable Long id, @RequestBody Usuarios usuarios){
        return ResponseEntity.ok(usuarioService.updateUsuario(id, usuarios));
    }

    @GetMapping("usuarios/{id}")
    public ResponseEntity<Usuarios> getOneUser(@PathVariable Long id){
        return ResponseEntity.ok(usuarioService.getOneUser(id));
    }

    @DeleteMapping("usuarios/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable Long id){
        usuarioService.deleteUser(id);
        return ResponseEntity.ok("Usuario eliminado");
    }
}
